package ru.t1.malyugin.tm.unit.service;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.malyugin.tm.api.service.ITaskService;
import ru.t1.malyugin.tm.configuration.WebApplicationConfiguration;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.util.UserUtil;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class})
public class TaskServiceTest {

    private static final String USERNAME = "unitTester";
    private static final String PASSWORD = "unitTester";
    private final Task task1 = new Task("T1", "T1");
    private final Task task2 = new Task("T2", "T2");
    @Autowired
    private ITaskService taskService;
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, PASSWORD);
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        taskService.addForUser(UserUtil.getUserId(), task1);
        taskService.addForUser(UserUtil.getUserId(), task2);
    }

    @After
    public void clean() {
        taskService.clearForUser(UserUtil.getUserId());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, taskService.countForUser(UserUtil.getUserId()));
    }

    @Test
    public void findByIdTest() {
        final Task task = taskService.findByIdForUser(UserUtil.getUserId(), task1.getId());
        Assert.assertEquals(task1.getId(), task.getId());
        Assert.assertEquals(task1.getName(), task.getName());
        Assert.assertEquals(task1.getDescription(), task.getDescription());
        Assert.assertEquals(task1.getStatus(), task.getStatus());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(2, taskService.countForUser(UserUtil.getUserId()));
    }

    @Test
    public void deleteAll() {
        taskService.clearForUser(UserUtil.getUserId());
        Assert.assertEquals(0, taskService.countForUser(UserUtil.getUserId()));
    }

    public void deleteById() {
        Assert.assertNotNull(taskService.findByIdForUser(UserUtil.getUserId(), task1.getId()));
        taskService.deleteByIdForUser(UserUtil.getUserId(), task1.getId());
        Assert.assertNull(taskService.findByIdForUser(UserUtil.getUserId(), task1.getId()));
    }

    @Test
    public void editTest() {
        final Task task = taskService.findByIdForUser(UserUtil.getUserId(), task1.getId());
        task.setDescription("NEW D");
        task.setName("NEW N");
        taskService.editForUser(UserUtil.getUserId(), task);
        final Task newTask = taskService.findByIdForUser(UserUtil.getUserId(), task1.getId());
        Assert.assertEquals(task.getId(), newTask.getId());
        Assert.assertEquals(task.getName(), "NEW N");
        Assert.assertEquals(task.getDescription(), "NEW D");
    }

}