package ru.t1.malyugin.tm.unit.repository;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.malyugin.tm.configuration.WebApplicationConfiguration;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.repository.ProjectRepository;
import ru.t1.malyugin.tm.util.UserUtil;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class})
public class ProjectRepositoryTest {

    private static final String USERNAME = "unitTester";
    private static final String PASSWORD = "unitTester";
    private final Project project1 = new Project("P1", "P1");
    private final Project project2 = new Project("P2", "P2");
    private final Project project3 = new Project("P3", "P3");
    private final Project project4 = new Project("P4", "P4");
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, PASSWORD);
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        project1.setUserId(UserUtil.getUserId());
        project2.setUserId(UserUtil.getUserId());
        project3.setUserId(UserUtil.getUserId());
        project4.setUserId(UserUtil.getUserId());
        projectRepository.save(project1);
        projectRepository.save(project2);
    }

    @After
    public void clean() {
        projectRepository.deleteByUserId(UserUtil.getUserId());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, projectRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void findByIdTest() {
        final Project project = projectRepository.findByUserIdAndId(UserUtil.getUserId(), project1.getId()).orElse(null);
        Assert.assertEquals(project1.getId(), project.getId());
        Assert.assertEquals(project1.getName(), project.getName());
        Assert.assertEquals(project1.getDescription(), project.getDescription());
        Assert.assertEquals(project1.getStatus(), project.getStatus());
    }

    @Test
    public void countByUserTest() {
        Assert.assertEquals(2, projectRepository.countByUserId(UserUtil.getUserId()));
    }

    @Test
    public void deleteAllByUserIdTest() {
        projectRepository.deleteAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, projectRepository.countByUserId(UserUtil.getUserId()));
    }

    @Test
    public void deleteByIdTest() {
        Assert.assertNotNull(projectRepository.findByUserIdAndId(UserUtil.getUserId(), project1.getId()).orElse(null));
        projectRepository.deleteByUserIdAndId(UserUtil.getUserId(), project1.getId());
        Assert.assertNull(projectRepository.findByUserIdAndId(UserUtil.getUserId(), project1.getId()).orElse(null));
    }

    @Test
    public void existsByIdTest() {
        Assert.assertTrue(projectRepository.existsByUserIdAndId(UserUtil.getUserId(), project1.getId()));
        Assert.assertFalse(projectRepository.existsByUserIdAndId(UserUtil.getUserId(), project3.getId()));
    }

}