package ru.t1.malyugin.tm.enumerated;

import lombok.Getter;

@Getter
public enum RoleType {
    USER("User"),
    ADMINISTRATOR("Administrator"),
    GUEST("Guest");

    private final String displayName;

    RoleType(final String displayName) {
        this.displayName = displayName;
    }

}