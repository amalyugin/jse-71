package ru.t1.malyugin.tm.api.service;

import ru.t1.malyugin.tm.model.Project;

import java.util.Collection;

public interface IProjectService {

    Collection<Project> findAllForUser(String userId);

    long countForUser(String userId);

    void createForUser(String userId);

    void addForUser(String userId, Project project);

    void deleteByIdForUser(String userId, String id);

    void clearForUser(String userId);

    void editForUser(String userId, Project project);

    Project findByIdForUser(String userId, String id);

}