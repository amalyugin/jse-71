package ru.t1.malyugin.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.malyugin.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
public final class Task implements Serializable {

    private static final long serialVersionUID = 1;

    @Id
    private String id = UUID.randomUUID().toString();

    @Column(name = "start_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date start = new Date();

    @Column(name = "finish_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date finish;

    @Column(name = "name")
    private String name = "";

    @Column(name = "description")
    private String description = "";

    @Column(name = "status")
    private Status status = Status.NOT_STARTED;

    @Column(name = "project_id")
    private String projectId;

    @Column(name = "tm_user")
    private String userId;

    public Task(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

}