package ru.t1.malyugin.tm.endpoint.soap;

import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.malyugin.tm.api.endpoint.IProjectSoapEndpoint;
import ru.t1.malyugin.tm.api.service.IProjectService;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.util.UserUtil;

import javax.jws.WebService;
import java.util.Collection;

@WebService(endpointInterface = "ru.t1.malyugin.tm.api.endpoint.IProjectSoapEndpoint")
public class ProjectSoapEndpoint implements IProjectSoapEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    public Project getById(final String id) {
        return projectService.findByIdForUser(UserUtil.getUserId(), id);
    }

    @Override
    public void create(final Project project) {
        projectService.addForUser(UserUtil.getUserId(), project);
    }

    @Override
    public void update(final Project project) {
        projectService.editForUser(UserUtil.getUserId(), project);
    }

    @Override
    public void deleteById(final String id) {
        projectService.deleteByIdForUser(UserUtil.getUserId(), id);
    }

    @Override
    public Collection<Project> getList() {
        return projectService.findAllForUser(UserUtil.getUserId());
    }

    @Override
    public long count() {
        return projectService.countForUser(UserUtil.getUserId());
    }

    @Override
    public void clearList() {
        projectService.clearForUser(UserUtil.getUserId());
    }

}