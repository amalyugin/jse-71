package ru.t1.malyugin.tm.endpoint.soap;

import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.malyugin.tm.api.endpoint.ITaskSoapEndpoint;
import ru.t1.malyugin.tm.api.service.ITaskService;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.util.UserUtil;

import javax.jws.WebService;
import java.util.Collection;

@WebService(endpointInterface = "ru.t1.malyugin.tm.api.endpoint.ITaskSoapEndpoint")
public class TaskSoapEndpoint implements ITaskSoapEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    public Task getById(final String id) {
        return taskService.findByIdForUser(UserUtil.getUserId(), id);
    }

    @Override
    public void create(final Task task) {
        taskService.addForUser(UserUtil.getUserId(), task);
    }

    @Override
    public void update(final Task task) {
        taskService.editForUser(UserUtil.getUserId(), task);
    }

    @Override
    public void deleteById(final String id) {
        taskService.deleteByIdForUser(UserUtil.getUserId(), id);
    }

    @Override
    public Collection<Task> getList() {
        return taskService.findAllForUser(UserUtil.getUserId());
    }

    @Override
    public long count() {
        return taskService.countForUser(UserUtil.getUserId());
    }

    @Override
    public void clearList() {
        taskService.clearForUser(UserUtil.getUserId());
    }

}