package ru.t1.malyugin.tm.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.malyugin.tm.model.Task;

@FeignClient(name = "tasksClient")

public interface TasksRestEndpointClient {

    String URL = "http://localhost:8080/api/tasks";

    static TasksRestEndpointClient getInstance() {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TasksRestEndpointClient.class, URL);
    }

    @GetMapping("/get")
    Iterable<Task> get();

    @GetMapping("/count")
    long count();

    @DeleteMapping("/delete")
    void delete(@RequestBody Iterable<Task> tasks);

    @DeleteMapping("/delete/all")
    void clear();

}