package ru.t1.malyugin.tm.api.endpoint;

import ru.t1.malyugin.tm.model.Result;
import ru.t1.malyugin.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IAuthSoapEndpoint {

    @WebMethod
    Result login(
            @WebParam(name = "username", partName = "username") String username,
            @WebParam(name = "password", partName = "password") String password
    );

    @WebMethod
    Result logout();

    @WebMethod
    User profile();

}