package ru.t1.malyugin.tm.service;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.malyugin.tm.api.service.IUserService;
import ru.t1.malyugin.tm.enumerated.RoleType;
import ru.t1.malyugin.tm.model.Role;
import ru.t1.malyugin.tm.model.User;
import ru.t1.malyugin.tm.repository.UserRepository;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

@Service
public class UserService implements IUserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @Override
    @Transactional
    public void create(
            final String login,
            final String password,
            final RoleType roleType
    ) {
        if (StringUtils.isBlank(login)) return;
        if (StringUtils.isBlank(password)) return;
        if (userRepository.existsByLogin(login)) return;
        final String passwordHash = passwordEncoder.encode(password);
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        System.out.println("------------ID: " + user.getId());
        final Role role = new Role();
        role.setUser(user);
        role.setRoleType(roleType);
        user.setRoles(Collections.singletonList(role));
        userRepository.save(user);
    }

    @Override
    public void create(User user) {
        if (user == null) return;
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void edit(
            final User user
    ) {
        if (user == null) return;
        userRepository.save(user);
    }

    @Override
    public Optional<User> findByLogin(
            final String login
    ) {
        if (StringUtils.isBlank(login)) return Optional.empty();
        return userRepository.findByLogin(login);
    }

    @Override
    public Optional<User> findById(final String id) {
        if (StringUtils.isBlank(id)) return Optional.empty();
        return userRepository.findById(id);
    }

    @Override
    public boolean existsByLogin(
            final String login
    ) {
        if (StringUtils.isBlank(login)) return false;
        return userRepository.existsByLogin(login);
    }

    @Override
    public Collection<User> findAll() {
        return userRepository.findAll();
    }

}