package ru.t1.malyugin.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import ru.t1.malyugin.tm.model.Project;

import java.util.Collection;
import java.util.Optional;

public interface ProjectRepository extends JpaRepository<Project, String> {

    Collection<Project> findAllByUserId(@Param("userId") String userId);

    Optional<Project> findByUserIdAndId(@Param("userId") String userId, @Param("id") String id);

    Optional<Project> findByIdAndUserId(String id, String userId);

    long countByUserId(@Param("userId") String userId);

    boolean existsByUserIdAndId(@Param("userId") String userId, @Param("id") String id);

    void deleteByUserIdAndId(@Param("userId") String userId, @Param("id") String id);

    void deleteAllByUserId(@Param("userId") String userId);

    void deleteByUserId(@Param("userId") String userId);

}