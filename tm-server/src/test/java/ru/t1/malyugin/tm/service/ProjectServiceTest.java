package ru.t1.malyugin.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.malyugin.tm.api.service.dto.IProjectDTOService;
import ru.t1.malyugin.tm.api.service.dto.IUserDTOService;
import ru.t1.malyugin.tm.configuration.ServerConfiguration;
import ru.t1.malyugin.tm.dto.model.ProjectDTO;
import ru.t1.malyugin.tm.enumerated.EntitySort;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.NameEmptyException;
import ru.t1.malyugin.tm.exception.field.UserIdEmptyException;
import ru.t1.malyugin.tm.marker.UnitCategory;

import java.util.List;
import java.util.stream.Collectors;

import static ru.t1.malyugin.tm.TestData.*;

@Category(UnitCategory.class)
public class ProjectServiceTest {

    private static IProjectDTOService PROJECT_DTO_SERVICE;

    private static IUserDTOService USER_DTO_SERVICE;

    @NotNull
    private static String userId1 = "";

    @NotNull
    private static String userId2 = "";

    @BeforeClass
    public static void setUp() throws LiquibaseException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);

        PROJECT_DTO_SERVICE = context.getBean(IProjectDTOService.class);
        USER_DTO_SERVICE = context.getBean(IUserDTOService.class);

        @NotNull final Liquibase liquibase = context.getBean(Liquibase.class);
        liquibase.dropAll();
        liquibase.update("scheme");

        userId1 = USER_DTO_SERVICE.create(FIRST_USUAL_USER_LOGIN, FIRST_USUAL_USER_PASS, null, Role.USUAL).getId();
        userId2 = USER_DTO_SERVICE.create(SECOND_USUAL_USER_LOGIN, SECOND_USUAL_USER_PASS, null, Role.USUAL).getId();
    }

    @AfterClass
    public static void tearDown() {
        USER_DTO_SERVICE.removeById(userId1);
        USER_DTO_SERVICE.removeById(userId2);
    }

    @Before
    public void initTest() {
        for (int i = 1; i <= NUMBER_OF_PROJECTS; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO("P" + i, "D" + i);
            if (i <= NUMBER_OF_PROJECTS / 2) project.setUserId(userId1);
            else project.setUserId(userId2);
            PROJECT_DTO_SERVICE.add(project);
            PROJECT_LIST.add(project);
        }
    }

    @After
    public void clearData() {
        PROJECT_DTO_SERVICE.clear();
        TASK_LIST.clear();
        PROJECT_LIST.clear();
    }

    @Test
    public void testClear() {
        final long expectedNumberOfEntries = 0;
        PROJECT_DTO_SERVICE.clear();
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_DTO_SERVICE.getSize());
    }

    @Test
    public void testClearForUser() {
        PROJECT_DTO_SERVICE.clear(UNKNOWN_ID);
        Assert.assertEquals(NUMBER_OF_PROJECTS, PROJECT_DTO_SERVICE.getSize());

        final int expectedNumberOfEntries = 0;
        PROJECT_DTO_SERVICE.clear(userId1);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_DTO_SERVICE.getSize(userId1));
        Assert.assertNotEquals(expectedNumberOfEntries, PROJECT_DTO_SERVICE.getSize(userId2));
    }

    @Test
    public void testClearForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.clear(null));
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(PROJECT_LIST.size(), PROJECT_DTO_SERVICE.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        final long actualProjectList = PROJECT_DTO_SERVICE.getSize(userId1);
        Assert.assertEquals(actualProjectList, PROJECT_DTO_SERVICE.getSize(userId1));
        Assert.assertEquals(0, PROJECT_DTO_SERVICE.getSize(UNKNOWN_ID));
    }

    @Test
    public void testGetSizeForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.getSize(null));
    }

    @Test
    public void testRemoveByIdForUser() {
        final int expectedNumberOfEntries = NUMBER_OF_PROJECTS - 1;
        @Nullable final ProjectDTO project = PROJECT_LIST.get(0);
        Assert.assertNotNull(project);
        PROJECT_DTO_SERVICE.removeById(userId1, project.getId());
        Assert.assertNull(PROJECT_DTO_SERVICE.findOneById(userId1, project.getId()));
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_DTO_SERVICE.getSize());
    }

    @Test
    public void testFindOneByIdForUser() {
        Assert.assertNull(PROJECT_DTO_SERVICE.findOneById(UNKNOWN_ID, UNKNOWN_ID));
        for (@NotNull final ProjectDTO project : PROJECT_DTO_SERVICE.findAll(userId1))
            Assert.assertEquals(project.getId(), PROJECT_DTO_SERVICE.findOneById(userId1, project.getId()).getId());
    }

    @Test
    public void testFindOneByIdForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.findOneById(null, UNKNOWN_ID));
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<ProjectDTO> actualProjectList = PROJECT_DTO_SERVICE.findAll(userId1);
        @NotNull final List<ProjectDTO> expectedProjectList = PROJECT_LIST
                .stream()
                .filter(p -> userId1.equals(p.getUserId()))
                .collect(Collectors.toList());
        Assert.assertEquals(expectedProjectList.size(), actualProjectList.size());
        for (int i = 0; i < expectedProjectList.size(); i++) {
            Assert.assertEquals(expectedProjectList.get(i).getId(), actualProjectList.get(i).getId());
        }
    }

    @Test
    public void testFindAllForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.findAll((String) null));
    }

    @Test
    public void testFindAllWithSortForUser() {
        @NotNull final List<ProjectDTO> actualProjectList = PROJECT_DTO_SERVICE.findAll(userId1);

        PROJECT_DTO_SERVICE.changeStatusById(userId1, actualProjectList.get(0).getId(), Status.IN_PROGRESS);
        PROJECT_DTO_SERVICE.changeStatusById(userId1, actualProjectList.get(4).getId(), Status.COMPLETED);

        Assert.assertEquals(Status.COMPLETED, PROJECT_DTO_SERVICE.findAll(userId1, EntitySort.BY_STATUS).get(0).getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, PROJECT_DTO_SERVICE.findAll(userId1, EntitySort.BY_STATUS).get(1).getStatus());

        PROJECT_DTO_SERVICE.updateById(userId1, actualProjectList.get(3).getId(), "A", null);
        PROJECT_DTO_SERVICE.updateById(userId1, actualProjectList.get(1).getId(), "B", null);

        Assert.assertEquals("A", PROJECT_DTO_SERVICE.findAll(userId1, EntitySort.BY_NAME).get(0).getName());
        Assert.assertEquals("B", PROJECT_DTO_SERVICE.findAll(userId1, EntitySort.BY_NAME).get(1).getName());

        @NotNull final List<ProjectDTO> expectedProjectList = PROJECT_DTO_SERVICE.findAll(userId1);
        Assert.assertEquals(expectedProjectList.size(), PROJECT_DTO_SERVICE.findAll(userId1, (EntitySort) null).size());
    }

    @Test
    public void testFindAllWithSortForUserForUser() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.findAll(null, EntitySort.BY_CREATED));
    }

    @Test
    public void testCrateProjectDTO() {
        final int expectedNumberOfEntries = NUMBER_OF_PROJECTS + 2;
        @NotNull final ProjectDTO project1 = PROJECT_DTO_SERVICE.create(userId1, "NAME1", "DESCRIPTION");
        @NotNull final ProjectDTO project2 = PROJECT_DTO_SERVICE.create(userId2, "NAME2", null);
        PROJECT_LIST.add(project1);
        PROJECT_LIST.add(project2);

        Assert.assertEquals(expectedNumberOfEntries, PROJECT_DTO_SERVICE.getSize());
        Assert.assertEquals("NAME1", project1.getName());
        Assert.assertEquals("DESCRIPTION", project1.getDescription());
        Assert.assertEquals(userId1, project1.getUserId());

        Assert.assertEquals("NAME2", project2.getName());
        Assert.assertEquals("", project2.getDescription());
        Assert.assertEquals(userId2, project2.getUserId());
    }

    @Test
    public void testCrateProjectNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.create(null, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(NameEmptyException.class, () -> PROJECT_DTO_SERVICE.create(UNKNOWN_ID, null, UNKNOWN_ID));
        Assert.assertThrows(NameEmptyException.class, () -> PROJECT_DTO_SERVICE.create(UNKNOWN_ID, null, null));
    }

    @Test
    public void testUpdateById() {
        @NotNull final ProjectDTO project = PROJECT_DTO_SERVICE.findAll(userId1).get(0);
        PROJECT_DTO_SERVICE.updateById(userId1, project.getId(), "NEW N", "NEW D");
        @Nullable final ProjectDTO actualProjectDTO = PROJECT_DTO_SERVICE.findOneById(userId1, project.getId());
        Assert.assertNotNull(actualProjectDTO);
        Assert.assertEquals("NEW N", actualProjectDTO.getName());
        Assert.assertEquals("NEW D", actualProjectDTO.getDescription());
    }

    @Test
    public void testUpdateByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.updateById(null, UNKNOWN_ID, UNKNOWN_STRING, UNKNOWN_STRING));
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_DTO_SERVICE.updateById(UNKNOWN_ID, null, UNKNOWN_STRING, UNKNOWN_STRING));
        Assert.assertThrows(ProjectNotFoundException.class, () -> PROJECT_DTO_SERVICE.updateById(UNKNOWN_ID, UNKNOWN_ID, UNKNOWN_STRING, UNKNOWN_STRING));
    }

    @Test
    public void testChangeProjectStatusById() {
        @NotNull final ProjectDTO project = PROJECT_DTO_SERVICE.findAll(userId1).get(0);
        PROJECT_DTO_SERVICE.changeStatusById(userId1, project.getId(), Status.COMPLETED);
        @Nullable ProjectDTO actualProjectDTO = PROJECT_DTO_SERVICE.findOneById(userId1, project.getId());
        Assert.assertNotNull(actualProjectDTO);
        Assert.assertEquals(Status.COMPLETED, actualProjectDTO.getStatus());
        PROJECT_DTO_SERVICE.changeStatusById(userId1, project.getId(), null);
        actualProjectDTO = PROJECT_DTO_SERVICE.findOneById(userId1, project.getId());
        Assert.assertNotNull(actualProjectDTO);
        Assert.assertEquals(Status.COMPLETED, actualProjectDTO.getStatus());
    }

    @Test
    public void testChangeProjectStatusByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.changeStatusById(null, UNKNOWN_ID, Status.COMPLETED));
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_DTO_SERVICE.changeStatusById(UNKNOWN_ID, null, Status.COMPLETED));
    }

    @Test
    public void testRemoveByIdForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_DTO_SERVICE.removeById(null, UNKNOWN_ID));
    }

}