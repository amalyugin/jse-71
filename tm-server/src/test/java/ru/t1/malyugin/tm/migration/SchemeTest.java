package ru.t1.malyugin.tm.migration;

import liquibase.exception.LiquibaseException;
import org.junit.Test;

public class SchemeTest extends AbstractSchemeTest {

    @Test
    public void schemeTest() throws LiquibaseException {
        LIQUIBASE.dropAll();
        LIQUIBASE.update("scheme");
    }

}