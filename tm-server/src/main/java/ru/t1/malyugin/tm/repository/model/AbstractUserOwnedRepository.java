package ru.t1.malyugin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.malyugin.tm.model.AbstractUserOwnedModel;
import ru.t1.malyugin.tm.model.User;

import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> {

    List<M> findAllByUser(@NotNull User user);

    Optional<M> findByUserAndId(@NotNull User user, @NotNull String id);

    long countByUser(@NotNull User user);

    void deleteByUser(@NotNull User user);

    void deleteByUserAndId(@NotNull User user, @NotNull String id);

    boolean existsByUserAndId(@NotNull User user, @NotNull String id);

}