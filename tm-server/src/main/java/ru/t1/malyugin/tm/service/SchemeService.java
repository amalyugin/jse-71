package ru.t1.malyugin.tm.service;

import liquibase.Liquibase;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.malyugin.tm.api.service.ISchemeService;
import ru.t1.malyugin.tm.api.service.property.IPropertyService;
import ru.t1.malyugin.tm.exception.user.PermissionException;

@Service
public final class SchemeService implements ISchemeService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final Liquibase liquibase;

    @Autowired
    public SchemeService(
            @NotNull final IPropertyService propertyService,
            @NotNull final Liquibase liquibase) {
        this.propertyService = propertyService;
        this.liquibase = liquibase;
    }

    @Override
    @SneakyThrows
    public void dropScheme(@Nullable final String initToken) {
        if (StringUtils.isBlank(initToken)) throw new PermissionException();
        @NotNull final String token = propertyService.getLiquibaseInitToken();
        if (!StringUtils.equals(initToken, token)) throw new PermissionException();
        liquibase.dropAll();
        liquibase.close();
    }

    @Override
    @SneakyThrows
    public void updateScheme(@Nullable final String initToken) {
        if (StringUtils.isBlank(initToken)) throw new PermissionException();
        @NotNull final String token = propertyService.getLiquibaseInitToken();
        if (!StringUtils.equals(initToken, token)) throw new PermissionException();
        liquibase.update("scheme");
        liquibase.close();
    }

}