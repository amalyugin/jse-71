package ru.t1.malyugin.tm.configuration;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.t1.malyugin.tm.api.service.property.IPropertyService;

import javax.jms.ConnectionFactory;

@Configuration
public class JmsConfiguration {

    @Bean
    @NotNull
    public ConnectionFactory jmsConnectionFactory(@NotNull final IPropertyService propertyService) {
        @NotNull final String url = propertyService.getJmsQueueUrl();
        @Nullable ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);

        return connectionFactory;
    }

}