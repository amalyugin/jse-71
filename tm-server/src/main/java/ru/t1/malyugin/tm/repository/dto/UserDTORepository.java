package ru.t1.malyugin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.malyugin.tm.dto.model.UserDTO;

import java.util.Optional;

@Repository
@Scope("prototype")
public interface UserDTORepository extends AbstractDTORepository<UserDTO> {

    @NotNull
    Optional<UserDTO> findByLogin(@NotNull String login);

    @NotNull
    Optional<UserDTO> findByEmail(@NotNull String email);

    boolean existsByEmail(@NotNull String email);

    boolean existsByLogin(@NotNull String login);

    void removeByLogin(@NotNull String login);

    void removeByEmail(@NotNull String email);

    @Modifying
    @Query("UPDATE UserDTO u SET u.locked = :flag WHERE u.login = :login")
    void setLockFlagByLogin(@NotNull @Param("login") String login,
                            @Param("flag") boolean flag);

    @Modifying
    @Query("UPDATE UserDTO u SET u.passwordHash = :pass WHERE u.id = :id")
    void setPasswordById(@NotNull @Param("id") String id,
                         @NotNull @Param("pass") String pass);

}