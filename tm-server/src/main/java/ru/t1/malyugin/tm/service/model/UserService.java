package ru.t1.malyugin.tm.service.model;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.malyugin.tm.api.service.model.IUserService;
import ru.t1.malyugin.tm.api.service.property.IPropertyService;
import ru.t1.malyugin.tm.exception.field.EmailEmptyException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.LoginEmptyException;
import ru.t1.malyugin.tm.exception.field.PasswordEmptyException;
import ru.t1.malyugin.tm.exception.user.UserNotFoundException;
import ru.t1.malyugin.tm.model.User;
import ru.t1.malyugin.tm.repository.model.UserRepository;
import ru.t1.malyugin.tm.util.HashUtil;

@Service
public class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final UserRepository userRepository;

    public UserService(
            @NotNull final IPropertyService propertyService,
            @NotNull final UserRepository userRepository
    ) {
        this.propertyService = propertyService;
        this.userRepository = userRepository;
    }

    @NotNull
    @Override
    protected UserRepository getRepository() {
        return userRepository;
    }

    @Override
    @Transactional
    public void lockUser(@Nullable final String login) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        if (!userRepository.existsByLogin(login.trim())) throw new UserNotFoundException();
        getRepository().setLockFlagByLogin(login.trim(), true);
    }

    @Override
    @Transactional
    public void unlockUser(@Nullable final String login) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        if (!userRepository.existsByLogin(login.trim())) throw new UserNotFoundException();
        getRepository().setLockFlagByLogin(login.trim(), false);
    }

    @Override
    @Transactional
    public void removeByLogin(@Nullable final String login) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        if (!userRepository.existsByLogin(login.trim())) throw new UserNotFoundException();
        getRepository().removeByLogin(login.trim());
    }

    @Override
    @Transactional
    public void removeByEmail(@Nullable final String email) {
        if (StringUtils.isBlank(email)) throw new EmailEmptyException();
        getRepository().removeByEmail(email.trim());
    }

    @Override
    @Transactional
    public void setPassword(@Nullable final String id, @Nullable final String password) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        if (StringUtils.isBlank(password)) throw new PasswordEmptyException();
        if (!userRepository.existsById(id.trim())) throw new UserNotFoundException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) throw new PasswordEmptyException();
        getRepository().setPasswordById(id.trim(), passwordHash);
    }

    @Override
    @Transactional
    public void update(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName
    ) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final User user = findOneById(id.trim());
        if (user == null) throw new UserNotFoundException();
        if (!StringUtils.isBlank(firstName)) user.setFirstName(firstName.trim());
        if (!StringUtils.isBlank(lastName)) user.setLastName(lastName.trim());
        if (!StringUtils.isBlank(middleName)) user.setMiddleName(middleName.trim());
        update(user);
    }

    @Nullable
    @Override
    public User findOneByEmail(@Nullable final String email) {
        if (StringUtils.isBlank(email)) throw new EmailEmptyException();
        return getRepository().findByEmail(email.trim()).orElse(null);
    }

    @Nullable
    @Override
    public User findOneByLogin(@Nullable final String login) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        return getRepository().findByLogin(login.trim()).orElse(null);
    }

    @Override
    public boolean isEmailExist(@Nullable String email) {
        if (StringUtils.isBlank(email)) throw new EmailEmptyException();
        return getRepository().existsByEmail(email.trim());
    }

    @Override
    public boolean isLoginExist(@Nullable String login) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        return getRepository().existsByLogin(login.trim());
    }

}