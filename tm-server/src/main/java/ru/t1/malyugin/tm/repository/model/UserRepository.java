package ru.t1.malyugin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.malyugin.tm.model.User;

import java.util.Optional;

@Repository
@Scope("prototype")
public interface UserRepository extends AbstractRepository<User> {

    @NotNull
    Optional<User> findByLogin(@NotNull String login);

    @NotNull
    Optional<User> findByEmail(@NotNull String email);

    boolean existsByEmail(@NotNull String email);

    boolean existsByLogin(@NotNull String login);

    void removeByEmail(@NotNull String email);

    void removeByLogin(@NotNull String login);

    @Modifying
    @Query("UPDATE User SET locked = :flag WHERE login = :login")
    void setLockFlagByLogin(@NotNull @Param("login") String login,
                            @Param("flag") boolean flag);

    @Modifying
    @Query("UPDATE User SET passwordHash = :pass WHERE id = :id")
    void setPasswordById(@NotNull @Param("id") String id,
                         @NotNull @Param("pass") String pass);

}