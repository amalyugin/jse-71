package ru.t1.malyugin.tm.service.model;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.malyugin.tm.api.service.model.IService;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.model.AbstractModel;
import ru.t1.malyugin.tm.repository.model.AbstractRepository;

import java.util.List;

public abstract class AbstractService<M extends AbstractModel> implements IService<M> {

    @NotNull
    protected abstract AbstractRepository<M> getRepository();

    @Override
    @Transactional
    public void add(@NotNull final M model) {
        getRepository().saveAndFlush(model);
    }

    @Override
    @Transactional
    public void remove(@NotNull final M model) {
        getRepository().delete(model);
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        getRepository().saveAndFlush(model);
    }

    @Override
    public long getSize() {
        return getRepository().count();
    }

    @Override
    @Transactional
    public void clear() {
        getRepository().deleteAll();
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        return getRepository().findById(id.trim()).orElse(null);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        getRepository().deleteById(id.trim());
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return getRepository().findAll();
    }

}