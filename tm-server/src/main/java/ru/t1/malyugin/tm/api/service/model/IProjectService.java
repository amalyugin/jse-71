package ru.t1.malyugin.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.model.Project;

public interface IProjectService extends IWBSService<Project> {

    void updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    Project create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    boolean existsById(
            @Nullable String userId,
            @Nullable String id
    );

}