package ru.t1.malyugin.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.model.AbstractWBSDTOModel;
import ru.t1.malyugin.tm.enumerated.EntitySort;
import ru.t1.malyugin.tm.enumerated.Status;

import java.util.List;

public interface IWBSDTOService<M extends AbstractWBSDTOModel> extends IUserOwnedDTOService<M> {

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable EntitySort sort);

    void changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

}