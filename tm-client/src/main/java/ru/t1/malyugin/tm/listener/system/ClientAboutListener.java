package ru.t1.malyugin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.event.ConsoleEvent;

@Component
public final class ClientAboutListener extends AbstractSystemListener {

    @NotNull
    private static final String NAME = "about-client";

    @NotNull
    private static final String DESCRIPTION = "Client about";

    @NotNull
    private static final String ARGUMENT = "-a";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@clientAboutListener.getName() == #event.message")
    public void handleConsoleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[CLIENT INFORMATION]");
        System.out.println("NAME: " + propertyService.getApplicationName());
        System.out.println("AUTHOR NAME: " + propertyService.getAuthorName());
        System.out.println("AUTHOR E-MAIL: " + propertyService.getAuthorEmail());
    }

}