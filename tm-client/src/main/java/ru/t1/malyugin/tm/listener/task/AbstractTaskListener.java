package ru.t1.malyugin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.malyugin.tm.api.endpoint.ITaskEndpoint;
import ru.t1.malyugin.tm.dto.model.TaskDTO;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.listener.AbstractListener;

import java.util.List;

public abstract class AbstractTaskListener extends AbstractListener {

    @NotNull
    @Autowired
    protected ITaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void renderTaskList(@Nullable final List<TaskDTO> tasks) {
        int index = 1;
        if (tasks == null) return;
        for (@Nullable final TaskDTO task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task);
            index++;
        }
    }

    protected void renderTask(@Nullable final TaskDTO task) {
        if (task == null) return;
        System.out.println(task);
    }

}
