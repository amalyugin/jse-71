package ru.t1.malyugin.tm.dto.request.scheme;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.request.AbstractRequest;

@Getter
@Setter
@NoArgsConstructor
public final class SchemeUpdateRequest extends AbstractRequest {

    @Nullable
    private String initToken;

    public SchemeUpdateRequest(@Nullable final String initToken) {
        this.initToken = initToken;
    }

}