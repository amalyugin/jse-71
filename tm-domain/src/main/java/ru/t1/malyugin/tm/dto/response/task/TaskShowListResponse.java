package ru.t1.malyugin.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.model.TaskDTO;
import ru.t1.malyugin.tm.dto.response.AbstractResponse;

import java.util.List;


@Getter
@Setter
@NoArgsConstructor
public class TaskShowListResponse extends AbstractResponse {

    @Nullable
    private List<TaskDTO> taskList;

    public TaskShowListResponse(@Nullable final List<TaskDTO> taskList) {
        this.taskList = taskList;
    }

}